package com.kyriba.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class KafkaApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(KafkaApplication.class, args);

        final BIKafkaProducer producer = context.getBean(BIKafkaProducer.class);
        final String topic = "HedgeAllocation";
        final HedgeAllocation hedgeAllocation = HedgeAllocation.newBuilder()
                .setTransactionNumber("TR1")
                .setHedgeRelationshipNumber("HR1")
                .setTransactionAmount(1000D)
                .setAllocatedAmount(500D)
                .setCustomerId("CMP11")
                .build();
        producer.sendMessage(topic, hedgeAllocation);
    }

}
